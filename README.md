# Ulambda Scheme
<!--
  Copyright (C) 2018 Mike Gerwitz

  This file is part of Ulambda Scheme.

  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty provided the copyright notice and
  this notice are preserved.  This file is offered as-is, without warranty
  of any kind.
-->

**This project is very incomplete, has stalled, and is unlikely to be
completed.**

Ulambda Scheme (also abbreviated "Y Scheme") is a self-hosting Scheme.
The primary compiler target is JavaScript.

```
\\    //      \\\
 \\  //        \\\
  \\// Ulambda  \\\
   \\\   Scheme ///
    \\\        ///
     \\\      ///
```

## Bootstrapping
Ulambda is designed to be bootstrappable by anyone in the environment in
which it is designed to run: the web browser.  Alternatively, it can be
bootstrapped using Node.js.

### Via Web Browser
Simply visit `bootstrap/bootstrap.html` in a modern browser and follow the
provided instructions.

You can also view it at <https://mikegerwitz.com/ulambda/bootstrap.html>.

### Via Node.js
```
$ ./autogen.sh && ./configure && make
```


## Current State
As mentioned above, this project has stalled.  The current state of
development is summarized in the Viability Test Suite in
[`bootstrap/rebirth/test.scm`](./bootstrap/rebirth/test.scm).

