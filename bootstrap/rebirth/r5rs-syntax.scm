;;; Syntatic forms for R⁵RS Scheme
;;;
;;;  Copyright (C) 2018 Mike Gerwitz
;;;
;;;  This file is part of Ulambda Scheme.
;;;
;;;  Ulambda Scheme is free software: you can redistribute it and/or modify
;;;  it under the terms of the GNU Affero General Public License as
;;;  published by the Free Software Foundation, either version 3 of the
;;;  License, or (at your option) any later version.
;;;
;;;  This program is distributed in the hope that it will be useful,
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;  GNU General Public License for more details.
;;;
;;;  You should have received a copy of the GNU Affero General Public License
;;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; These are syntatic forms suitable for use in `(null-environment 5)'.
;;;
;;; This will eventually be shared between Rebirth and Ulambda.  It assumes
;;; that macro support exists.

;; convention: %% compiler internal

(define-macro (%%apply-procedure proc-name . args))
(define-macro (%%apply-?-procedure proc-name . args))

(define-macro (%%apply-macro macro-name . args))
(define-macro (%%apply-?-macro macro-name . args))



(define-macro (lambda fnargs . body)
  (`quote
   (PROC (unquote fnargs) (unquote body))))

(define-macro (let bindings . body)
  (if (empty? bindings)
      body
      (list (quote LET)
            (caar bindings)
            (cadr (car bindings))
            (let (cdr bindings) body))))

   (define-es-macro (let* bindings . body)
     "(function(){\n"
     (join "" (map (lambda (binding)
                     (string-append
                      "let " (tparam->es (car binding))  ; TODO: BC; remove
                      " = " (env-ref (car binding))
                      " = " (sexp->es (cadr binding)) ";\n"))
                   bindings))
     (body->es body #t) "\n"
     "    })()")
