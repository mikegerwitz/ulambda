;; Per-directory local variables for GNU Emacs 23 and later.

((scheme-mode
  . ((indent-tabs-mode . nil)
     (eval . (put 'es:while            'scheme-indent-function 0))
     (eval . (put 'topic               'scheme-indent-function 1))
     (eval . (put 'verify              'scheme-indent-function 1))
     (eval . (put 'skip                'scheme-indent-function 1))
     (eval . (put 'expect              'scheme-indent-function 1)))))
