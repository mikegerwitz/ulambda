/**
 * Command-line bootstrap procedure for Ulambda Scheme
 *
 *  Copyright (C) 2017 Mike Gerwitz
 *
 *  This file is part of Ulambda Scheme.
 *
 *  Ulambda Scheme is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This script is intended to be run from the command line and requires
 * Node.js; it is more suitable to development and automated processes.
 * If you are a user or non-developer without such an environment set up,
 * then you might rather bootstrap using your web browser instead; you can
 * do so by directing your web browser to `bootstrap.html', located in the
 * same directory as this file.
 *
 * To perform the entire bootstrapping process, simply invoke this script:
 *   $ node bootstrap.js
 *
 * For more information on each aspect of the process, see the individual
 * files in `./bootstrap/'.
 */

const Bootstrap = require( './Bootstrap' );
const Prebirth  = require( './prebirth' );

const logf = ( str, e ) =>
{
    console.log( str );

    if ( e instanceof Error ) {
        console.error( e );
    }
};

const getf = path => Promise.resolve(
    require( 'fs' ).readFileSync( `./${path}` ).toString()
);

const strap = new Bootstrap( getf, logf, new Prebirth() );

// Attempt bootstrap.  Error output is handled by Bootstrap, since the
// output must also work correctly in a browser environment.
strap.bootstrap()
    .catch( _ => process.exit( 1 ) );
